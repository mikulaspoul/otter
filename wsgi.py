"""
WSGI config for otter project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
import sentry_sdk
from sentry_sdk.integrations.wsgi import SentryWsgiMiddleware

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

from django.conf import settings

sentry_sdk.init(dsn=settings.SENTRY_DSN)

application = SentryWsgiMiddleware(get_wsgi_application())
