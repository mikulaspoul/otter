import json
from io import StringIO

import twitter
from dateutil.parser import parse
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.storage import default_storage
from django.http import JsonResponse, HttpResponseBadRequest, HttpResponse
from django.shortcuts import redirect
from django.views import View
from django.views.generic import FormView
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from sentry_sdk import capture_exception

from apps.tweets.forms import UploadExportForm
from apps.tweets.models import Tweet, TotalTweets


class UploadExportView(LoginRequiredMixin, FormView):

    form_class = UploadExportForm
    template_name = "upload_export.html"

    def dispatch(self, request, *args, **kwargs):
        if request.user and request.user.is_authenticated:
            try:
                if request.user.total_tweets:
                    messages.error(request, _("You've already imported your tweets."))
                    return redirect("index")
            except ObjectDoesNotExist:
                pass
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        file = form.cleaned_data["export"]

        text = file.read().decode("utf-8")

        try:
            text = text.replace("window.YTD.tweet.part0 = ", "")
            data = json.loads(text)
        except ValueError:
            filename = default_storage.save(f"import_{timezone.now().isoformat()}.js", StringIO(text))  # noqa:
            capture_exception()
            messages.error(self.request, _("Invalid format of the export file."))
            return redirect("upload_export")

        tweets = []

        for tweet in data:
            tweets.append(Tweet(user=self.request.user, tweet_id=tweet["id"], tweeted=parse(tweet["created_at"])))

        Tweet.objects.bulk_create(tweets, batch_size=100)

        TotalTweets.objects.create(user=self.request.user, total=len(tweets))

        messages.success(self.request, _("Tweets were uploaded, you can start the review."))

        return redirect("index")


def get_api(user):
    auth_data = user.social_auth.get()
    api = twitter.Api(
        consumer_key=settings.SOCIAL_AUTH_TWITTER_KEY,
        consumer_secret=settings.SOCIAL_AUTH_TWITTER_SECRET,
        access_token_key=auth_data.extra_data["access_token"]["oauth_token"],
        access_token_secret=auth_data.extra_data["access_token"]["oauth_token_secret"],
    )

    return api


class CurrentView(LoginRequiredMixin, View):
    def get(self, request):
        try:
            total_tweets = request.user.total_tweets
        except ObjectDoesNotExist:
            return HttpResponseBadRequest()

        api = get_api(request.user)

        while True:
            try:
                tweet = Tweet.objects.filter(user=request.user).earliest("tweeted")
            except ObjectDoesNotExist:
                return JsonResponse(
                    {"tweet": None, "total": total_tweets.total, "processed": total_tweets.total, "progress": 100}
                )
            else:
                try:
                    api.GetStatus(tweet.tweet_id)
                    break
                except twitter.error.TwitterError as e:  # doesn't exist anymore
                    message = e.message[0]

                    if message["code"] == 89:
                        return JsonResponse({"expired": True})
                    elif message["code"] in {144, 179}:
                        tweet.delete()
                    else:
                        capture_exception()
                        return JsonResponse({"error": message}, status=500)

        total = total_tweets.total
        processed = total_tweets.total - Tweet.objects.filter(user=request.user).count()
        progress = round(processed / total * 100, 2)

        return JsonResponse(
            {"tweet": str(tweet.tweet_id), "total": total_tweets.total, "processed": processed, "progress": progress}
        )


class KeepView(LoginRequiredMixin, View):
    def get(self, request):
        try:
            tweet = Tweet.objects.get(user=request.user, tweet_id=request.GET["tweet_id"])
        except (KeyError, ObjectDoesNotExist):
            return HttpResponseBadRequest()

        tweet.delete()

        return HttpResponse()


class DeleteView(LoginRequiredMixin, View):
    def get(self, request):
        try:
            tweet = Tweet.objects.get(user=request.user, tweet_id=request.GET["tweet_id"])
        except (KeyError, ObjectDoesNotExist):
            return HttpResponseBadRequest()

        api = get_api(request.user)

        api.DestroyStatus(tweet.tweet_id)

        tweet.delete()

        return HttpResponse()
