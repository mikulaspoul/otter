from django.db import models


class TotalTweets(models.Model):

    user = models.OneToOneField("auth.User", on_delete=models.CASCADE, related_name="total_tweets")
    total = models.PositiveIntegerField()


class Tweet(models.Model):

    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    tweet_id = models.BigIntegerField()
    tweeted = models.DateTimeField()
