from django.contrib import admin

from apps.tweets.models import Tweet, TotalTweets


class TweetAdmin(admin.ModelAdmin):
    list_display = ["username", "tweet_id", "tweeted"]
    list_filter = ["user"]
    list_select_related = ["user"]

    def username(self, obj):
        return obj.user.username


admin.site.register(Tweet, TweetAdmin)
admin.site.register(TotalTweets)
