from django import forms
from django.core.validators import FileExtensionValidator


class UploadExportForm(forms.Form):

    export = forms.FileField(label="Export file", validators=[FileExtensionValidator(allowed_extensions=["js"])])
